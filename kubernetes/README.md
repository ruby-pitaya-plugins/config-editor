Kubernetes Configuration
========================

Set namespace
-------------

Open files "deployment-*.yaml", "role-rubypitaya.yaml" and "rolebinding-rubypitaya.yaml" and set the correct namespace on fields and links


Apply sequence
--------------

If you already has a postgres don't apply the postgres files and change the
deployment-rubypitaya.yaml postgres env vars.
Run the following command on the listed files.
But remember to put your namespace and your KUBECONFIG env var if you need.

```sh
$ kubectl apply -f [file-path]
```

persistent-volume.yaml

statefulset-etcd.yaml
statefulset-nats.yaml
statefulset-postgres.yaml
statefulset-redis.yaml

service-etcd.yaml
service-nats.yaml
service-postgres.yaml
service-redis.yaml

deployment-connector.yaml
service-connector.yaml


Create gitlab-registry secret
-----------------------------

Run the following command.
But remember to put your namespace and your KUBECONFIG env var if you need.

```sh
$ kubectl create secret docker-registry gitlab-registry --docker-server=[registry-server] --docker-username=[username] --docker-password=[password]
```

If you use a different registry name instead of "gitlab-registry" change the deployment-rubypitaya.yaml imagePullSecrets.name


Apply sequence
--------------

Run the following command on the listed files.
But remember to put your namespace and your KUBECONFIG env var if you need.

```sh
$ kubectl apply -f [file-path]
```

deployment-rubypitaya.yaml
service-rubypitaya.yaml


Setup nginx for gameserver
--------------------------

Run the following command to create configmap with nginx.conf file.
But remember to put your namespace and your KUBECONFIG env var if you need.

```sh
$ kubectl create configmap nginx-config --from-file=./gameplay/nginx.conf
```

Create the deployments and services for nginx:

```sh
$ kubectl apply -f ./gameplay/deployment-nginx.yaml
$ kubectl apply -f ./gameplay/service-nginx.yaml
```