module ConfigEditor

  class ConfigBLL
    
    def get_all_configs_ordered_by_path
      ConfigEditor::Config.all.order(:path)
    end

    def get_config(config_id)
      ConfigEditor::Config.find_by_id(config_id)
    end

    def get_all_config_versions_without_json(config_id)
      ConfigEditor::ConfigVersion.where(config_id: config_id).select(:id, :name, :activated)
    end

    def activate(config_version_id)
      config_version = ConfigEditor::ConfigVersion.find_by_id(config_version_id)
      return if config_version.nil?

      deactivate_all(config_version.config_id)
      config_version.activated = true
      config_version.save!
      config_version
    end

    def deactivate(config_version_id)
      config_version = ConfigEditor::ConfigVersion.find_by_id(config_version_id)
      return if config_version.nil?

      config_version.activated = false
      config_version.save!
      config_version
    end

    def deactivate_all(config_id)
      ConfigEditor::ConfigVersion.where(config_id: config_id, activated: true).update_all(activated: false)
    end

    def create_config(path)
    end
  end
end