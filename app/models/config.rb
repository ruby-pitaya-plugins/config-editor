require 'active_record'

module ConfigEditor

  class Config < ActiveRecord::Base

    has_many :versions, class_name: 'ConfigVersion', dependent: :delete_all

    validates :path, presence: true
    validates :path, uniqueness: true

    def activated_version
      @activated_version.reload unless @activated_version.nil?
      @activated_version = nil unless @activated_version.nil? || @activated_version.activated

      return @activated_version unless @activated_version.nil? 

      @activated_version = ConfigEditor::ConfigVersion.where(config_id: id).where(activated: true).limit(1).first
      @activated_version
    end
  end
end