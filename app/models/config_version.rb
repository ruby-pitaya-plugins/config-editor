require 'json'
require 'active_record'

module ConfigEditor

  class ConfigVersion < ActiveRecord::Base

    belongs_to :config

    validates :json, presence: true
    validates :name, presence: true
    validates :config, presence: true
    validates :name, uniqueness: { scope: :config_id }

    validate :validate_json_format

    before_save :format_json

    def json_as_hash
      @json_as_hash ||= JSON.parse(json, symbolize_names: true)
      @json_as_hash
    end

    def json_pretty
      JSON.pretty_generate(json_as_hash)
    rescue
      json
    end

    private

    def validate_json_format
      JSON.parse(json)
    rescue
      errors.add(:json, "Invalid format")
    end

    def format_json
      self[:json] = json.squish
    end
  end
end