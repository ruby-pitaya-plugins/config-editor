require 'rubypitaya/core/http_routes'

RubyPitaya::HttpRoutes.class_eval do

  get '/config-editor' do
    content_type 'text/html'
    @configs = @bll[:config_editor].get_all_configs_ordered_by_path

    erb :'config_editor/index'
  end

  get '/config-editor/config/new' do
    content_type 'text/html'
    @config = ConfigEditor::Config.new

    erb :'config_editor/new_config'
  end

  post '/config-editor/config/new' do
    content_type 'text/html'
    @config = ConfigEditor::Config.new(path: params[:path]) unless params[:path].nil?
    @config = ConfigEditor::Config.new if @config.nil?

    if @config.save
      redirect to('/config-editor')
    else
      erb :'config_editor/new_config'
    end
  end

  get '/config-editor/config/edit/:config_id' do
    content_type 'text/html'
    config_id = params[:config_id]
    @config = @bll[:config_editor].get_config(config_id)
    @config_versions = @bll[:config_editor].get_all_config_versions_without_json(config_id)

    erb :'config_editor/config'
  end

  post '/config-editor/config/edit/:config_id' do
    content_type 'text/html'
    config_id = params[:config_id]
    @config = @bll[:config_editor].get_config(config_id)
    @config_versions = @bll[:config_editor].get_all_config_versions_without_json(config_id)

    @config.path = params[:path]
    @config.save

    erb :'config_editor/config'
  end

  get '/config-editor/config/delete/:config_id' do
    content_type 'text/html'
    config_id = params[:config_id]
    @config = @bll[:config_editor].get_config(config_id)
    @config.destroy

    redirect to('/config-editor')
  end

  get '/config-editor/config-version/activate/:config_version_id' do
    content_type 'text/html'
    config_version_id = params[:config_version_id]
    config_version = @bll[:config_editor].activate(config_version_id)
    config_id = config_version.config_id

    redirect to("/config-editor/config/edit/#{config_id}")
  end

  get '/config-editor/config-version/deactivate/:config_version_id' do
    content_type 'text/html'
    config_version_id = params[:config_version_id]
    config_version = @bll[:config_editor].deactivate(config_version_id)
    config_id = config_version.config_id

    redirect to("/config-editor/config/edit/#{config_id}")
  end

  get '/config-editor/config-version/new/:config_id' do
    content_type 'text/html'
    config_id = params[:config_id]
    @config = @bll[:config_editor].get_config(config_id)
    @config_version = ConfigEditor::ConfigVersion.new

    erb :'config_editor/new_config_version'
  end

  post '/config-editor/config-version/new/:config_id' do
    content_type 'text/html'
    config_id = params[:config_id]
    @config = @bll[:config_editor].get_config(config_id)

    @config_version = ConfigEditor::ConfigVersion.new(config_id: config_id,
                                                      name: params[:name],
                                                      json: params[:json])

    if @config_version.save
      redirect to("/config-editor/config/edit/#{config_id}")
    else
      erb :'config_editor/new_config_version'
    end
  end

  get '/config-editor/config-version/edit/:config_version_id' do
    content_type 'text/html'
    config_version_id = params[:config_version_id]
    @config_version = ConfigEditor::ConfigVersion.find_by_id(config_version_id)
    @config = @config_version.config

    erb :'config_editor/edit_config_version'
  end

  post '/config-editor/config-version/edit/:config_version_id' do
    content_type 'text/html'
    config_version_id = params[:config_version_id]
    @config_version = ConfigEditor::ConfigVersion.find_by_id(config_version_id)
    @config = @config_version.config

    @config_version.name = params[:name]
    @config_version.json = params[:json]

    if @config_version.save
      redirect to("/config-editor/config/edit/#{@config.id}")
    else
      erb :'config_editor/edit_config_version'
    end
  end

  get '/config-editor/config-version/delete/:config_version_id' do
    content_type 'text/html'
    config_version_id = params[:config_version_id]
    @config_version = ConfigEditor::ConfigVersion.find_by_id(config_version_id)
    @config = @config_version.config

    @config_version.delete

    redirect to("/config-editor/config/edit/#{@config.id}")
  end
end