module ConfigEditor
  class AppInitializer < RubyPitaya::InitializerBase

    # method:     run
    # parameter:  initializer_content
    # attributes:
    #  - bll
    #    - class: RubyPitaya::InstanceHolder
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/instance_holder.rb
    #    - methods:
    #      - add_instance(key, instance)
    #        - add any instance to any key
    #      - [](key)
    #        - get instance by key
    #  - redis
    #    - link: https://github.com/redis/redis-rb/
    #  - mongo
    #    - class: Mongo::Client
    #    - link: https://docs.mongodb.com/ruby-driver/current/tutorials/quick-start/
    #  - config
    #    - class: RubyPitaya::Config
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/config.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - setup
    #    - class: RubyPitaya::Setup
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/setup.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - log
    #    - class: Logger
    #    - link: https://ruby-doc.org/stdlib-2.6.4/libdoc/logger/rdoc/Logger.html
    #    - methods:
    #      - info
    #        - log information

    def run(initializer_content)
      initializer_content.config.config_core_override = ConfigEditor::ConfigCore.new

      bll = initializer_content.bll
      config_editor_bll = ConfigEditor::ConfigBLL.new()
      bll.add_instance(:config_editor, config_editor_bll)
    end

    def self.path
      __FILE__
    end
  end
end