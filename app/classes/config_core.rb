module ConfigEditor

  class ConfigCore

    def [](path)
      config_version = ConfigEditor::ConfigVersion.joins(:config).where('configs.path' => path).where(activated: true).limit(1).first

      return nil if config_version.nil?

      config_version.json_as_hash
    end

    def auto_reload
    end
  end
end