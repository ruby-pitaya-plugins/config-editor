module ConfigEditor

  class ConfigEditorHandler < RubyPitaya::HandlerBase

    non_authenticated_actions :getConfig

    def getConfig
      config_path = @params[:path]

      config_json = @config[config_path]

      response = {
        code: ConfigEditor::StatusCodes::CODE_OK,
        data: config_json
      }
    end
  end
end
