require 'active_record'

class CreateConfigMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :configs, id: :uuid do |t|
      t.string :path, null: false, unique: true

      t.timestamps null: false
    end

    add_index :configs, [:path, :id], unique: true
  end
end