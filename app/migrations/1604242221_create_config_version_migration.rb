require 'active_record'

class CreateConfigVersionMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :config_versions, id: :uuid do |t|
      t.belongs_to :config, type: :uuid, foreing_key: true

      t.string :name, null: false
      t.string :json, default: ""
      t.boolean :activated, default: false

      t.timestamps null: false
    end

    add_index :config_versions, [:name, :config_id], unique: true
  end
end