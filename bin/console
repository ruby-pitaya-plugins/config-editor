#!/usr/bin/env ruby

require 'bundler/setup'
require 'active_record'

require 'rubypitaya'
require 'rubypitaya/core/database_config'

# Database connection
database_config = RubyPitaya::DatabaseConfig.new
ActiveRecord::Base.establish_connection(database_config.connection_data)
ActiveRecord::Base.logger = ActiveSupport::Logger.new(STDOUT)
ActiveSupport::LogSubscriber.colorize_logging = true

# Loading core files
Gem.find_files('rubypitaya/**/*.rb').each do |path|
  require path unless path.end_with?('spec.rb') ||
                      path.include?('app/migrations') ||
                      path.include?('core/templates') ||
                      path.include?('core/spec-helpers') ||
                      path.include?('app-template')
end

# Loading application files
app_folder_paths = RubyPitaya::Path::Plugins::APP_FOLDER_PATHS + [RubyPitaya::Path::APP_FOLDER_PATH]
app_folder_paths.each do |app_folder_path|
  app_files_path = File.join(app_folder_path, '**/*.rb')

  Dir[app_files_path].each do |path|
    require path unless path.end_with?('spec.rb') ||
                        path.include?('app/migrations') ||
                        path.include?('spec_helper.rb')
  end
end

# Starting irb
require 'irb'
IRB.start(__FILE__)

# Closing database connection
ActiveRecord::Base.connection.close
