require 'rspec'
require 'cucumber/rspec/doubles'

ENV['RUBYPITAYA_ENV'] = 'test'

require 'rubypitaya/core/helpers/setup_helper'
require 'rubypitaya/core/spec-helpers/handler_spec_helper_class'