Given(/^[Cc]lient call route ["'](.+)["']$/) do |route|
  @handler_helper.request(route)
end

Given(/^[Ss]erver should response ["'](.+)["'] as ["'](.+)["']$/) do |response_key, expected_value|
  response_value = @handler_helper.response.dig(*response_key.split('.').map(&:to_sym))

  expect(response_value.to_s).to eq(expected_value)
end

Given(/^[Ss]erver should response the following json[:]*$/) do |expected_json|
  expected_json = JSON.generate(JSON.parse(expected_json.strip))
  response_json = JSON.generate(@handler_helper.response)

  expect(response_json).to eq(expected_json)
end

Given(/^[Ss]etup key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @handler_helper.add_setup(key, value)
end

Given(/^[Cc]onfig key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @handler_helper.add_config(key, value)
end

Given(/^[Cc]onfig is the following json[:]*$/) do |config_json|
  config = JSON.parse(config_json)
  @handler_helper.set_config(config)
end

Given(/^[Pp]rint server response$/) do 
  puts "response: #{@handler_helper.response}"
end

Given(/^(?:[Nn]ow\s)?[Tt]oday is ["'](.+)["']$/) do |date_text|
  time_now = date_text.to_time

  Time.stub(:now).and_return(time_now)
end

Given(/^[Tt]he [Uu]ser ["'](.+)["'] is authenticated$/) do |user_id|
  @handler_helper.authenticate(user_id)
end

Given(/^[Tt]he following [Uu]ser[s]*[:]*$/) do |table|
  table.hashes.each do |table_hash|
    User.create(id: table_hash[:user_id])
  end
end

Before do
  ActiveRecord::Base.descendants.each { |c| c.delete_all unless c == ActiveRecord::SchemaMigration }
  @handler_helper = RubyPitaya::HandlerSpecHelperClass.new('cucumber')
end

# After do
# end