Given(/^[Tt]he [Pp]layer ["'](.+)["'] is authenticated$/) do |player_name|
  player = Player.find_by_name(player_name)
  @handler_helper.authenticate(player.user_id)
end

Given(/^[Tt]he following [Pp]layer[s]*[:]*$/) do |table|
  player_hashes = table.hashes
  player_hashes.each do |player_hash|
    player_hash[:user] = User.new(id: player_hash[:user_id])
    Player.create(player_hash)
  end
end
