IMAGE_TAG ?= latest
IMAGE_REGISTRY ?= [put-your-registry-here]
KUBE_NAMESPACE ?= [put-your-namespace-here]
KUBE_DEPLOYMENT_SERVER ?= rubypitaya
KUBECONTEXT ?= ''

## + Server Commands

## Run ruby pitaya metagame project
run:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rubypitaya run

## Build project
build:
	@docker-compose build

## Run ruby irb console
console:
	@docker-compose run --service-ports --rm rubypitaya-console bundle exec ruby ./bin/console

## Run bash on container
bash:
	@docker-compose run --service-ports --rm rubypitaya bash
## Kill all containers
kill:
	@docker rm -f $$(docker-compose ps -aq)

## + Test Commands

## Run all tests
test: test-rspec test-cucumber

## Run rspec tests. FILE=spec.rb:10
test-rspec:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rspec $(FILE)

## Run cucumber tests. FILE=something.feature
test-cucumber:
	@docker-compose run --service-ports --rm rubypitaya bundle exec cucumber $(FILE)

## Update gems dependencies on Gemfile.lock
update-dependencies:
	@rm -f Gemfile.lock
	@docker run --rm -v "$(PWD)":/usr/src/app -w /usr/src/app ruby:2.7.2 bundle install

## + Improve metagame

## Create new migrgation. NAME=[migration-name]
create-migration:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rubypitaya create-migration $(NAME)

## Add or update a plugin. GIT=[plugin-http-git-link]
add-plugin:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rubypitaya add-plugin $(GIT)

## + Database Commands

## Create database
db-create:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:create

## Run migrations to database
db-migrate:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:migrate

## Show migrations status on database
db-migrate-status:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:status

## Rollback migrations STEP=1
db-rollback:
	@docker-compose run --service-ports --rm -e STEP="$(STEP)" rubypitaya-commands bundle exec rake db:rollback

## Drop database
db-drop:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:drop

## Reset database
db-reset:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:reset

## Setup test database
db-test-setup:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:test:setup

## + Deployment commands

## Build image to production environment
prod-build-image:
	@docker build . -f docker/prod/Dockerfile -t $(IMAGE_REGISTRY):$(IMAGE_TAG)

## Push prod image to gitlab
prod-push-image:
	@docker push $(IMAGE_REGISTRY):$(IMAGE_TAG)

## Deploy prod image to kubernetes cluster
prod-deploy-image:
	kubectl --context='$(KUBECONTEXT)' -n $(KUBE_NAMESPACE) set image deployment $(KUBE_DEPLOYMENT_SERVER) $(KUBE_DEPLOYMENT_SERVER)=$(IMAGE_REGISTRY):$(IMAGE_TAG)

.DEFAULT_GOAL := show-help

.PHONY: show-help
show-help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		if (length($$1) == 0) { \
		    printf "\n"; \
		} \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'



